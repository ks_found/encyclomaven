# EncycloMaven

Maven repository for EncycloEngine and EncycloCrawler. Hosted at https://encyclosearch.org/maven.

## Add the EncycloEngine API or the EncycloCrawler API to your project

### Gradle
```groovy
repositories {
    ...
    maven {
        url "https://encyclosearch.org/maven"
    }
}

dependencies {
    ...
    // EncycloEngine
    implementation group: 'org.encyclosphere', name: 'encycloengine', version: '0.1.0'

    // EncycloCrawler
    implementation group: 'org.encyclosphere', name: 'encyclocrawler', version: '1.2'
}
```
